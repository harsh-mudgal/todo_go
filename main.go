package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

type Task struct {
	id          string
	title       string
	description string
	date        string
	isCompleted bool
}

func (t *Task) changeUser(newTask Task) { // method    t.changeUser(newTask)
	t.id = newTask.id // like functions inside a class
}

func main() {
	arr := [3]int{1, 2, 3} // a=b copy comperable
	fmt.Println(arr)

	var mp map[string]int // a=b reference so not conperable
	mp = map[string]int{"t": 1}
	print(mp)

	var tasks []Task
	tasks = []Task{{id: "1", title: "Test", description: "des", date: "17", isCompleted: false}}
	tasks = append(tasks, Task{id: "1", title: "Test", description: "des", date: "17", isCompleted: false})
	fmt.Println(tasks)

	for i := 1; i < 3; i++ {
		fmt.Println(i)
	}

	for key, value := range tasks {
		fmt.Println(key, value)
	}
	http.HandleFunc("/", Handler)
	http.ListenAndServe(":8080", nil)
}

func Handler(w http.ResponseWriter, r *http.Request) {
	f, _ := os.Open("./tasks.txt")
	io.Copy(w, f)
}
